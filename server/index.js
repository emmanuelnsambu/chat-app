const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;

io.on('connection', socket => {
  console.log('user connected');

  // Handles the user joining process
  socket.on('join', data => {
    socket.join(data.room);
    socket.in(data.room)
      .emit(
        'user-joined',
        {
          user: data.user,
          message: 'has joined',
          image: 'assets/images/avatar.png',
          time: new Date().getHours() + ':' + new Date().getMinutes()
        });
  });

  // Handles the user leaving process
  socket.on('leave', data => {
    socket.leave(data.room, err => {
      socket.in(data.room)
        .emit(
          'user-left',
          {
            user: data.user,
            message: 'has left',
            image: 'assets/images/avatar.png',
            time: new Date().getHours() + ':' + new Date().getMinutes()
          });
    });
  });

  // Handles the user message process
  socket.on('message', data => {
    io.in(data.room).emit(
      'new-message',
      {
        user: data.user,
        message: data.message,
        image: 'assets/images/avatar.png',
        time: new Date().getHours() + ':' + new Date().getMinutes()
      });
  });
});

http.listen(port, () => {
  console.log(`started on port: ${port}`);
});